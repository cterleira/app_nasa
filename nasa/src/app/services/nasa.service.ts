import { DatePipe } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { Planet } from "../models/planet.model";
import { throwError } from "rxjs";
import { catchError, take, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class NasaService {
  private baseUrlApod = environment.BASE_URL_APOD;
  private apiKey = environment.NASA_KEY;
  public showSpinner: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: HttpClient, public datepipe: DatePipe) {}

  /*
   * Servicio para traer array de los planetas con fecha HOY y 6 días atras
   **/
  public getPlanets(): Observable<Planet[]> {
    let date = new Date();
    let start_date = this.datepipe.transform(date, "yyyy-MM-dd");
    let end_date = this.datepipe.transform(
      date.setDate(date.getDate() - 5),
      "yyyy-MM-dd"
    );
    const apodUrl =
      this.baseUrlApod +
      "start_date=" +
      end_date +
      "&end_date=" +
      start_date +
      "&api_key=" +
      this.apiKey;
    this.showSpinner.next(true);
    return this.http.get<Planet[]>(apodUrl).pipe(
      tap(
        (response) => this.showSpinner.next(false),
        (error: any) => this.showSpinner.next(false)
      ),
      catchError((err: any) => {
        return throwError("Error: ", err);
      })
    );
  }

  /*
   * Servicio para traer planeta con fecha HOY
   **/
  public getPlanetDate(date: string): Observable<Planet> {
    const datePlanet = date;
    const apodUrl =
      this.baseUrlApod + "date=" + datePlanet + "&api_key=" + this.apiKey;
    this.showSpinner.next(true);
    return this.http.get<Planet>(apodUrl).pipe(
      take(1),
      tap(
        (response) => this.showSpinner.next(false),
        (error: any) => this.showSpinner.next(false)
      ),
      catchError((err: any) => {
        return throwError("Error: ", err);
      })
    );
  }
}
