import { TestBed } from "@angular/core/testing";
import {
  HttpTestingController,
  HttpClientTestingModule,
} from "@angular/common/http/testing";
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { DatePipe } from "@angular/common";

import { NasaService } from "./nasa.service";
import { Planet } from "../models/planet.model";

import { environment } from "src/environments/environment";

const listPlanet: Planet[] = [
  {
    copyright: "",
    date: "",
    explanation: "",
    hdurl: "",
    media_type: "",
    service_version: "",
    title: "",
    url: "",
  },
  {
    copyright: "",
    date: "",
    explanation: "",
    hdurl: "",
    media_type: "",
    service_version: "",
    title: "",
    url: "",
  },
  {
    copyright: "",
    date: "",
    explanation: "",
    hdurl: "",
    media_type: "",
    service_version: "",
    title: "",
    url: "",
  },
];

const planet: Planet = {
  copyright: "",
  date: "",
  explanation: "",
  hdurl: "",
  media_type: "",
  service_version: "",
  title: "",
  url: "",
};

describe("NasaService", () => {
  let service: NasaService;
  let httpMock: HttpTestingController;
  let storage = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NasaService, DatePipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    service = TestBed.get(NasaService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterAll(() => {
    httpMock.verify();
  });

  it("getPlanets return a list of planet", () => {
    /*let start_date = { param: 'start_date', value: '2021-09-07' };
    let end_date = { param: 'end_date', value: '2021-09-11' };
    let api_key = { param: 'api_key', value: environment.NASA_KEY };

    const  urlMock = `${environment.BASE_URL_APOD}${start_date.param}=${start_date.value}&${end_date.param}=${end_date.value}&${api_key.param}=${api_key.value}`;
    service.getPlanets().subscribe((resp: Planet[]) => {
      expect(resp).toEqual(listPlanet);
    });

    const req = httpMock.expectOne(urlMock);
    expect(req.request.method).toBe("GET");
    req.flush('Get');*/

  });

  it("getPlanet return planet", () => {
    const start_date = "2021-09-12";
    const urlMock =
      environment.BASE_URL_APOD +
      "date=2021-09-12&api_key=" +
      environment.NASA_KEY;
    service.getPlanetDate(start_date).subscribe((resp: Planet) => {
      expect(resp).toEqual(planet);
    });

    const req = httpMock.expectOne(urlMock);
    expect(req.request.method).toBe("GET");
    req.flush(planet);
  });

  it("should be created", () => {
    const service: NasaService = TestBed.get(NasaService);
    expect(service).toBeTruthy();
  });
});
