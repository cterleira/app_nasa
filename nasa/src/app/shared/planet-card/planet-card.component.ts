import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Planet } from 'src/app/models/planet.model';

@Component({
  selector: 'app-planet-card',
  templateUrl: './planet-card.component.html',
  styleUrls: ['./planet-card.component.css']
})
export class PlanetCardComponent implements OnInit {

  @Input() planet: Planet;

  constructor(public router: Router) { }

  ngOnInit() {
  }

  /*
  * Enviamos al detalle del planeta por fecha
  */
  detailtPlanet() {
    this.router.navigate(['/detail/' + this.planet.date]);
  }

}
