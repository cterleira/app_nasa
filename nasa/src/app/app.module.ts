import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClient } from "@angular/common/http";
import { DatePipe } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PlanetModule } from "./modules/planet.module";
import { NasaService } from "./services/nasa.service";


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PlanetModule
  ],
  providers: [DatePipe,HttpClient,NasaService],
  bootstrap: [AppComponent],
})
export class AppModule {}
