import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Planet } from "../../models/planet.model";
import { NasaService } from "../../services/nasa.service";

@Component({
  selector: "app-planet-list",
  templateUrl: "./planet-list.component.html",
  styleUrls: ["./planet-list.component.css"],
})
export class PlanetListComponent implements OnInit {
  public listPlanet: Planet[];

  constructor(private nasaService: NasaService, public router: Router) {}

  ngOnInit() {
    this.getPlanets();
  }

  /*
  * Obtener listado de planetas del servicio
  */
  getPlanets() {
    this.nasaService.getPlanets().subscribe((response: Planet[]) => {
      this.listPlanet = response;
    });
  }

}
