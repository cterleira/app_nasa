import { DatePipe } from "@angular/common";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { NasaService } from "src/app/services/nasa.service";
import { RouterTestingModule } from "@angular/router/testing";
import { PlanetListComponent } from "./planet-list.component";
import { Planet } from "src/app/models/planet.model";
import { of } from "rxjs";
import { PlanetCardComponent } from "src/app/shared/planet-card/planet-card.component";

const listPlanet: Planet[] = [
  {
    copyright: "",
    date: "",
    explanation: "",
    hdurl: "",
    media_type: "",
    service_version: "",
    title: "",
    url: "",
  },
  {
    copyright: "",
    date: "",
    explanation: "",
    hdurl: "",
    media_type: "",
    service_version: "",
    title: "",
    url: "",
  },
  {
    copyright: "",
    date: "",
    explanation: "",
    hdurl: "",
    media_type: "",
    service_version: "",
    title: "",
    url: "",
  },
];

const planetServiceMock = {
  getPlanets: () => of(listPlanet),
};

describe("PlanetListComponent", () => {
  let component: PlanetListComponent;
  let fixture: ComponentFixture<PlanetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [ {
        provide: NasaService,
        useValue: planetServiceMock
    }, DatePipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      declarations: [PlanetListComponent,PlanetCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("getPlanet get planet from the subscription", () => {
    component.getPlanets();
    expect(component.listPlanet.length).toBe(3);
  });
});
