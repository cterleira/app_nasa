import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Planet } from "src/app/models/planet.model";
import { NasaService } from "src/app/services/nasa.service";

@Component({
  selector: "app-planet-detail",
  templateUrl: "./planet-detail.component.html",
  styleUrls: ["./planet-detail.component.css"],
})
export class PlanetDetailComponent implements OnInit {
  public listPlanet: Planet[];
  public detailPlanet: Planet;

  constructor(
    private nasaService: NasaService,
    public router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getPlanetDate();
  }

  /*
   * Obtener detalle de planeta del servicio
   */
  getPlanetDate(): void {
    const id = this.route.snapshot.paramMap.get("id");
    this.nasaService.getPlanetDate(id).subscribe((response: Planet) => {
      this.detailPlanet = response;
    });
  }
}
