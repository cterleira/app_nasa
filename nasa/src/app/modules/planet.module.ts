import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanetRoutingModule } from './planet-routing.module';
import { PlanetCardComponent } from '../shared/planet-card/planet-card.component';
import { PlanetListComponent } from './planet-list/planet-list.component';
import { SpinnerComponent } from '../shared/spinner/spinner.component';
import { PlanetDetailComponent } from './planet-detail/planet-detail.component';
import { PlanetComponent } from './planet.component';
import { MatButtonModule, MatCardModule, MatIconModule, MatProgressSpinnerModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    PlanetDetailComponent,
    PlanetCardComponent,
    SpinnerComponent,
    PlanetComponent,
    PlanetListComponent
  ],
  imports: [
    CommonModule,
    PlanetRoutingModule,
    HttpClientModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [
    PlanetComponent
  ]
})
export class PlanetModule { }
